from ctypes import CDLL, \
    c_uint32, \
    c_uint8, \
    c_void_p, \
    c_char_p, \
    c_bool


def get_dll():
    import os, fnmatch
    __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
    file_dll = os.path.join(__location__,"xaiwann.so")
    if not os._exists(file_dll):
        for root, dirs, files in os.walk(__location__):
            for name in files:
                if fnmatch.fnmatch(name, "xaiwann*.so"):
                    file_dll = os.path.join(root, name)
                    return file_dll
    return file_dll

flib = CDLL(get_dll())


native_version = flib.version
native_version.argtypes = []
native_version.restype = c_char_p

# FRWI

native_frwi_create = flib.frwi_create
native_frwi_create.argtypes = [c_uint32,c_uint32,c_uint32,c_bool]
native_frwi_create.restype = c_void_p

native_frwi_destroy = flib.frwi_destroy
native_frwi_destroy.argtypes = [c_void_p]
native_frwi_destroy.restype = None

native_frwi_getTotalOfLocalPermutations = flib.frwi_getTotalOfLocalPermutations
native_frwi_getTotalOfLocalPermutations.argtypes = [c_void_p]
native_frwi_getTotalOfLocalPermutations.restype = c_uint32

native_frwi_getLMISize = flib.frwi_getLMISize
native_frwi_getLMISize.argtypes = [c_void_p]
native_frwi_getLMISize.restype = c_uint32

# native_frwi_generateLocalPermutations = flib.frwi_generateLocalPermutations
# native_frwi_generateLocalPermutations.argtypes = [c_void_p,c_void_p,c_uint32,c_void_p,c_uint8]
# native_frwi_generateLocalPermutations.restype = None

native_frwi_generatePermutations = flib.frwi_generatePermutations
native_frwi_generatePermutations.argtypes = [c_void_p,c_void_p,c_uint32,c_void_p,c_uint8]
native_frwi_generatePermutations.restype = None

native_frwi_explain = flib.frwi_explain
native_frwi_explain.argtypes = [c_void_p,c_void_p,c_void_p,c_void_p,c_uint32]
native_frwi_explain.restype = None