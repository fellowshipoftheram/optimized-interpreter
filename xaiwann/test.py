from unittest import TestCase, main
import xaiwann as xw
import numpy as np


class FRWITestCase(TestCase):

    def setUp(self):
        self.height = 28
        self.width = 28
        self.window =  4
        self.greyValue = 0
        self.samples = 1000
        self.img = np.ones(self.height*self.width,dtype=np.uint8)
        self.imgRGB = np.ones(self.height*self.width*3,dtype=np.uint8)
        self.c = np.ones(10,dtype=np.float32)
        self.responses = np.ones((self.samples,self.c.size),dtype=np.float32)

    def test_build(self):
        try:
            frwi = xw.FRWI(self.window,self.height,self.width)
            self.assertIsInstance(frwi,xw.FRWI)
        except TypeError:
            self.fail("FRWI instantiation failed!")

    def test_buildRGB(self):
        try:
            frwi = xw.FRWI(self.window,self.height,self.width,isRGB=True)
            self.assertIsInstance(frwi,xw.FRWI)
        except TypeError:
            self.fail("FRWI RGB instantiation failed!")
    
    # def test_generateLocalPermutations(self):
    #     try:
    #         frwi = xw.FRWI(self.window,self.height,self.width)
    #         data = frwi.generateLocalPermutations(self.img, self.greyValue)
    #     except TypeError:
    #         self.fail("FRWI generateLocalPermutations failed!")

    # def test_generateLocalPermutationsRGB(self):
    #     try:
    #         frwi = xw.FRWI(self.window,self.height,self.width,isRGB=True)
    #         data = frwi.generateLocalPermutations(self.imgRGB, self.greyValue)
    #     except TypeError:
    #         self.fail("FRWI RGB generateLocalPermutations failed!")

    def test_generatePermutations(self):
        try:
            frwi = xw.FRWI(self.window,self.height,self.width)
            data = frwi.generatePermutations(self.img, self.greyValue, self.samples)
        except TypeError:
            self.fail("FRWI generatePermutations failed!")

    def test_generatePermutationsRGB(self):
        try:
            frwi = xw.FRWI(self.window,self.height,self.width,isRGB=True)
            data = frwi.generatePermutations(self.imgRGB, self.greyValue, self.samples)
        except TypeError:
            self.fail("FRWI RGB generatePermutations failed!")

    # def test_explain_local(self):
    #     try:
    #         frwi = xw.FRWI(self.window,self.height,self.width)
    #         data = frwi.generateLocalPermutations(self.img, self.greyValue)
    #         lmi = frwi.explain(self.responses,self.c)
    #     except TypeError:
    #         self.fail("FRWI explain local permutations failed!")

    # def test_explain_localRGB(self):
    #     try:
    #         frwi = xw.FRWI(self.window,self.height,self.width,isRGB=True)
    #         data = frwi.generateLocalPermutations(self.imgRGB, self.greyValue)
    #         lmi = frwi.explain(self.responses,self.c)
    #     except TypeError:
    #         self.fail("FRWI RGB explain local permutations failed!")

    def test_explain(self):
        try:
            frwi = xw.FRWI(self.window,self.height,self.width)
            data = frwi.generatePermutations(self.img, self.greyValue, self.samples)
            lmi = frwi.explain(self.responses,self.c)
        except TypeError:
            self.fail("FRWI explain permutations failed!")
    
    def test_explainRGB(self):
        try:
            frwi = xw.FRWI(self.window,self.height,self.width,isRGB=True)
            data = frwi.generatePermutations(self.imgRGB, self.greyValue, self.samples)
            lmi = frwi.explain(self.responses,self.c)
        except TypeError:
            self.fail("FRWI RGB explain permutations failed!")

    def test_full(self):
        try:
            frwi = xw.FRWI(2,5,5)
            img = np.array([
                [0,255,255,0,0],
                [0,255,255,0,0],
                [0,0,255,255,0],
                [0,0,255,255,0],
                [0,0,0,255,255],
            ],dtype=np.uint8)
            data = frwi.generatePermutations(img, self.greyValue, self.samples)
            c = np.array(np.random.uniform(size=4),dtype=np.float32)
            responses = np.array([np.random.uniform(size=4) for i in range(self.samples)],dtype=np.float32)
            lmi = frwi.explain(responses,c)
        except TypeError:
            self.fail("FRWI explain permutations failed!")

    def test_fullRGB(self):
        try:
            frwi = xw.FRWI(2,5,5,isRGB=True)
            img = np.array([
                [0,0,0,255,255,255,255,255,255,0,0,0,0,0,0],
                [0,0,0,255,255,255,255,255,255,0,0,0,0,0,0],
                [0,0,0,0,0,0,255,255,255,255,255,255,0,0,0],
                [0,0,0,0,0,0,255,255,255,255,255,255,0,0,0],
                [0,0,0,0,0,0,0,0,0,255,255,255,255,255,255],
            ],dtype=np.uint8)
            data = frwi.generatePermutations(img, self.greyValue, self.samples)
            c = np.array(np.random.uniform(size=4),dtype=np.float32)
            responses = np.array([np.random.uniform(size=4) for i in range(self.samples)],dtype=np.float32)
            lmi = frwi.explain(responses,c)
        except TypeError:
            self.fail("FRWI explain permutations failed!")

if __name__ == '__main__':
    main(verbosity=2)