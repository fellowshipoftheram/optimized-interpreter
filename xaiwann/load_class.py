from .load_native import *
from ctypes import ArgumentError
import numpy as np

__version__ = native_version()

class Main:
    INVALID_ARGUMENTS = "Arguments types invalids!"
    def __del__(self):
        self.close()

    def close(self):
        if self.ptr:
            self.destroy(self.ptr)
            self.ptr=None

    def validate(self):
        if getattr(self,'ptr',None) is None:
            raise RuntimeError("class point is null!")
        if getattr(self,'destroy',None) is None:
            raise RuntimeError("function destroy not found!")

class FRWI(Main):
    def __init__(self, window, height, width, isRGB=False):
        if type(window) is not int or type(height) is not int or type(width) is not int or type(isRGB) is not bool:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        self.ptr = native_frwi_create(window,height,width,isRGB)
        self.destroy = native_frwi_destroy
        self.height = height
        self.width = width
    
    # def generateLocalPermutations(self, img, greyValue):
    #     self.validate()
    #     if type(greyValue) is not int or img.dtype != np.uint8:
    #         raise ArgumentError(self.INVALID_ARGUMENTS)
    #     samples = native_frwi_getTotalOfLocalPermutations(self.ptr)
    #     data = np.zeros((samples,img.size),dtype=np.uint8)
    #     native_frwi_generateLocalPermutations(self.ptr,data.ctypes.data,samples,img.ctypes.data,greyValue)
    #     return data
    
    def generatePermutations(self, img, greyValue, samples):
        self.validate()
        if type(greyValue) is not int or type(samples) is not int or img.dtype != np.uint8:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        data = np.zeros((samples,img.size),dtype=np.uint8)
        native_frwi_generatePermutations(self.ptr,data.ctypes.data,samples,img.ctypes.data,greyValue)
        return data
    
    def explain(self, responses, c):
        self.validate()
        if responses.dtype != np.float32 or c.dtype != np.float32:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        lmi = np.zeros(native_frwi_getLMISize(self.ptr),dtype=np.float64)
        native_frwi_explain(self.ptr,lmi.ctypes.data,responses.ctypes.data,c.ctypes.data,c.size)
        return lmi
        