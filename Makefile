.PHONY: xaiwann/xaiwann.so test.o
xaiwann/xaiwann.so:
	g++ -fPIC -shared wrapper/xaiwann.cc -o xaiwann/xaiwann.so -Wall -O3 -std=c++11

test: xaiwann/xaiwann.so
	python3 -m xaiwann.test

all: xaiwann/xaiwann.so

test.o:
	g++ src/test.cc -g -O1 -o test.o

testcpp: test.o
	./test.o

valgrind: test.o
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./test.o