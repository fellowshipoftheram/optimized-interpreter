# README #

eXaplainable Artificial Intelligence - Weightless Artificial Neural Network -- XAIWANN

### What is this repository for? ###

* This repository is a tool based on fuzzy logic and WiSARD that generates an interpretation's image of any classifier's classification that gives us a probabilistic vector as an output.
* For now, work over grey images, working on colourful images.
* 0.0.1

### How do I get set up? ###

* git clone https://bitbucket.org/fellowshipoftheram/optimized-interpreter.git
* pip3 install optimized-interpreter/

### Usage of this library ###

```python
import xaiwann as xw
import numpy as np

# This function bellow represents a generic classifier that returns a probabilistic vector.
def classifier(data):
    responses = np.array([[0.1,0.2,0.3,0.4]],dtype=np.float64)
    return responses

line = 28 # the number of lines of the image.
column = 28 # the number of columns of the image.
img = np.ones((28,29),dtype=np.float64)

window = 4 # the window that algorithm will operate over the input image. In this case, 4 lines x 4 columns.
frwi = xw.FRWI(window,line,column)

samples = 1000
greyValue = 0 # this is the value that will randomly write in the image.
# generate random images based on a given image.
data = frwi.generatePermutations(img, greyValue, samples)

# apply the classification function over the input image.
c = classifier(img)
# apply the classification function over the permutation images.
responses = classifier(data)


# generates the explanation of the classifier's classification over the input image.
lmi = frwi.explain(responses,c)

#lmi is the interpretation's image over the classifier. The values variates between 0 and 1, where 0 means no relevance and 1 completely relevant.
```

### Who do I talk to? ###

* Aluizio Lima Filho