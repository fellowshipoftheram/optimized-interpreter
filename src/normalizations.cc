void normBiggerLesser(double* mi, uint32_t length){
    double bigger = 0;
    double lesser = 0;

    bool firstPositive = true;

    for(unsigned int i=0; i!=length; i++) {
        if(firstPositive){
            firstPositive=false;
            lesser = mi[i];
            bigger = mi[i];
        }
        else{
            if( mi[i] < lesser ) lesser = mi[i];
            if( (mi[i]) > bigger ) bigger = mi[i];
        }
    }

    bigger -= lesser;

    for(unsigned int i=0; i!=length; i++) {
        if(bigger!=0){
            mi[i] = (mi[i]-lesser)/bigger;
        }
    }
}

inline double l2norm2v(float* x, float* y, uint32_t length){
    double accum = 0;
    for(uint32_t i=0; i!=length; i++){
        double v = x[i]-y[i];
        accum += v * v;
    }
    return sqrt(accum);
}

inline double l2norm2v(uint8_t* x, uint8_t* y, uint32_t length){
    double accum = 0;
    for(uint32_t i=0; i!=length; i++){
        double v = x[i]-y[i];
        accum += v * v;
    }
    return sqrt(accum);
}

inline double sum(float* x, uint32_t length){
    double accum = 0;
    for(uint32_t i=0; i!=length; i++) accum+=x[i];
    return accum;
}