#include "base.h"

#include "version.h"

#include "print.cc"

#include "random.cc"
#include "bin.cc"
#include "rew.cc"
#include "kmeans.cc"
#include "normalizations.cc"
#include "membership_functions.cc"
#include "fuzzy_operators.cc"
#include "mapping.cc"
#include "frwi.cc"