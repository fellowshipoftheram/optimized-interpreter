std::vector<uint32_t> getNeighborhoodMapping(uint32_t& tupleSize, int line, int column, int neighborSize){
    tupleSize = 2*neighborSize+1;
    tupleSize *= tupleSize;
    std::vector<uint32_t> mapping(line*column*tupleSize);
    uint32_t ii=0;
    for(int l=0; l!=line; l++){
        for(int c=0; c!=column; c++){
            for(int ll=(l-neighborSize); ll<=(l+neighborSize); ll++){
                for(int cc=(c-neighborSize); cc<=(c+neighborSize); cc++){
                    if(ll < 0 || cc < 0 || ll >= line || cc >= column)
                        mapping[ii++] = ( l*column + c);
                    else
                        mapping[ii++] = ( ll*column + cc );
                }
            }
        }
    }
    return mapping;
}