class FRWI {
public:
    FRWI(uint32_t window, uint32_t height, uint32_t width, bool isRGB=false)
    :window(window),height(height),width(width),isRGB(isRGB){
        mapping = getNeighborhoodMapping(tupleSize, height, width, 1);
    }

    ~FRWI(){
        cleanTemporaryData();
    }

    // void generateLocalPermutations(uint8_t* data, uint32_t samples, uint8_t* img, uint8_t greyValue){
    //     if(bins.size()>0) return;
    //     setTemporaryData(data,img,samples);
    //     clear(data, samples*getImgSize(), greyValue);

    //     uint32_t ii=0;
    //     for(uint32_t i=0; i!=(height-window); i++){
    //         for(uint32_t j=0; j!=(width-window); j++){
    //             bins[ii].setSize(height*width);
    //             paintFeature(bins[ii], &data[ii*getImgSize()], img, i, j);
    //             ii++;
    //         }
    //     }
    // }

    void generatePermutations(uint8_t* data, uint32_t samples, uint8_t* img, uint8_t greyValue){
        if(bins.size()>0) return;
        setTemporaryData(data,img,samples);
        clear(data, samples*getImgSize(), greyValue);

        double featureNumberBase = (height*width/(double)(window*window));
        for(uint32_t ii=0; ii<samples; ii+=2){
            int featureNumber = featureNumberBase * randdouble(0,1);
            bins[ii].setSize(height*width);
            for(int f=0; f!=featureNumber; f++)
                paintFeature(bins[ii], &data[ii*getImgSize()], img,
                    randint(0,height-window), 
                    randint(0,width-window));
            paintFeaturesOpposite(bins[ii+1], bins[ii], &data[(ii+1)*getImgSize()], img);
        }
    }

    void explain(double* localMentalImage, float* responses, float* c, uint32_t classes){
        RewMemory rewP(mapping,height*width,tupleSize);
        RewMemory rewN(mapping,height*width,tupleSize);

        double l2mc = sqrt(getImgSize()*255*255);
        std::vector<std::vector<double>> data(4,std::vector<double>(samplesTotal/2));

        // calculate factors
        uint32_t k = 0;
        for(uint32_t ii=0; ii!=samplesTotal; ii+=2){
            // relevance evaluation
            float* sample_c = &responses[ii*classes];
            if(sum(sample_c,classes)>0){
                data[0][k] = l2norm2v(c,sample_c, classes); // classification distance; [Fc]
            }
            else data[0][k] = 1;
            data[1][k] = l2norm2v(base_img,&samples_img[ii*getImgSize()],getImgSize())/l2mc; // noisy distance; [Fi]

            // opposite relevance evaluation
            sample_c = &responses[(ii+1)*classes];
            if(sum(sample_c,classes)>0){
                data[2][k] = l2norm2v(c,sample_c, classes); // classification distance; [Fc-1]
            }
            else data[2][k] = 1;
            data[3][k] = l2norm2v(base_img,&samples_img[(ii+1)*getImgSize()],getImgSize())/l2mc; // noisy distance; [Fi-1]
            k++;
        }

        // calculate clusters
        std::vector<std::vector<double>> clusters(4,{0.3,0.5,0.7});
        for(size_t j=0; j!=data.size(); j++){
            clusters[j] = kmeans(data[j], 3);
        }

        // calculate fuzzy output and train the rew
        for(size_t ii=0; ii!=data[0].size(); ii++){
            // 0 - [Fc]
            // 1 - [Fi]
            // 2 - [Fc-1]
            // 3 - [Fi-1]

            // positive
            // [Fc]^¬[Fc-1]^[Fi]
            double outputFuzzy = positiveRelevanceFuzzyRules(data[0][ii],data[2][ii],data[1][ii],clusters[0],clusters[2],clusters[1]);
            rewP.train(bins[ii*2],outputFuzzy);

            // [Fc-1]^¬[Fc]^[Fi-1]
            outputFuzzy = positiveRelevanceFuzzyRules(data[2][ii],data[0][ii],data[3][ii],clusters[2],clusters[0],clusters[3]);
            rewP.train(bins[ii*2+1],outputFuzzy);

            // negative
            // [Fc]^[Fc-1]^[Fi] v ¬[Fc]^¬[Fc-1]^[Fi]
            outputFuzzy = negativeRelevanceFuzzyRules(data[0][ii],data[2][ii],data[1][ii],clusters[0],clusters[2],clusters[1]);
            rewN.train(bins[ii*2],outputFuzzy);

            // [Fc]^[Fc-1]^[Fi-1] v ¬[Fc]^¬[Fc-1]^[Fi-1]
            outputFuzzy = negativeRelevanceFuzzyRules(data[2][ii],data[0][ii],data[3][ii],clusters[2],clusters[0],clusters[3]);
            rewN.train(bins[ii*2+1],outputFuzzy);
        }

        // generate local mental image and normalize it
        // positive
        rewP.getMentalImage(localMentalImage);
        normBiggerLesser(localMentalImage, height*width);
        // negative
        rewN.getMentalImage(&localMentalImage[height*width]);
        normBiggerLesser(&localMentalImage[height*width], height*width);

        // clear processing data
        cleanTemporaryData();
    }

    uint32_t getTotalOfLocalPermutations(){
        return (height-window)*(width-window);
    }

    uint32_t getLMISize(){
        return height*width*2;
    }

private:
    inline uint32_t getPixelSize(){
        return isRGB?3:1;
    }

    inline uint32_t getImgSize(){
        return height*width*getPixelSize();
    }

    inline std::vector<double> validate(std::vector<double> clusters){
        int s = 3;
        std::vector<double> c;
        
        for(int i=0; i!=s; i++){
            if(std::isnan(clusters[i])){
                if(clusters.size()==3){
                    return {0.3,0.5,0.7};
                }
                else{
                    return {0.3,0.4,0.5,0.6,0.7};
                }
            }
        }
        return clusters;
    }

    inline void clear(uint8_t* img, uint32_t length, uint8_t v){
        for(uint32_t i=0; i!=length; i++)
            img[i] = v;
    }

    inline void paintFeature(Bin& bin, uint8_t* sample_img, uint8_t* img, int i, int j){
        for(uint32_t k=0; k!=window; k++){
            for(uint32_t l=0; l!=window; l++){
                int ix = (i+k)*width*getPixelSize() + (j+l)*getPixelSize();
                bin.set((i+k)*width + (j+l),1);
                sample_img[ix] = img[ix];
                if(isRGB){
                    sample_img[ix+1] = img[ix+1];
                    sample_img[ix+2] = img[ix+2];
                }
            }
        }
    }

    inline void paintFeaturesOpposite(Bin& binOppose, Bin& bin, uint8_t* sample_img, uint8_t* img){
        binOppose.setSize(height*width);
        for(uint32_t k=0; k!=height; k++){
            for(uint32_t l=0; l!=width; l++){
                int i = k*width + l;
                if(bin[i]==0){
                    binOppose.set(i,1);
                    int ix = k*width*getPixelSize() + l*getPixelSize();
                    sample_img[ix] = img[ix];
                    if(isRGB){
                        sample_img[ix+1] = img[ix+1];
                        sample_img[ix+2] = img[ix+2];
                    }
                }
            }
        }
    }

    inline void setTemporaryData(uint8_t* data, uint8_t* img, uint32_t samples){
        base_img = img;
        samples_img = data;
        samplesTotal = samples;
        bins.resize(samples);
    }

    inline void cleanTemporaryData(){
        bins.clear();
        base_img = NULL;
        samples_img = NULL;
        samplesTotal = 0;
    }

    // inline double positiveFuzzyRules(double cd, double id, 
    // std::vector<double>& cCD, 
    // std::vector<double>& cID){
    //     double positiveRule = orF(
    //         {
    //             andF({mst3('L',cd,cCD),mst3('H',id,cID)}),
    //             andF({mst3('L',cd,cCD),mst3('M',id,cID)}),
    //             andF({mst3('L',cd,cCD),mst3('L',id,cID)})
    //         });
    //     double notPositiveRule = orF(
    //         {
    //             andF({mst3('H',cd,cCD),mst3('H',id,cID)}),
    //             andF({mst3('H',cd,cCD),mst3('M',id,cID)}),
    //             andF({mst3('H',cd,cCD),mst3('L',id,cID)})
    //         });
    //     double positive = positiveRule > notPositiveRule ? positiveRule-notPositiveRule : 0;
    //     return positive;
    // }

    inline double positiveRelevanceFuzzyRules(double cd, double cdi, double id, 
    std::vector<double>& cCD,
    std::vector<double>& cCDI,
    std::vector<double>& cID){
        double relevantRule = orF(
            {
                andF({mst3('L',cd,cCD),notF(mst3('L',cdi,cCDI)),mst3('H',id,cID)}),
                andF({mst3('L',cd,cCD),notF(mst3('L',cdi,cCDI)),mst3('M',id,cID)}),
                andF({mst3('L',cd,cCD),notF(mst3('L',cdi,cCDI)),mst3('L',id,cID)})
            });
        double notRelevantRule = orF(
            {
                andF({mst3('H',cd,cCD),notF(mst3('H',cdi,cCDI)),mst3('H',id,cID)}),
                andF({mst3('H',cd,cCD),notF(mst3('H',cdi,cCDI)),mst3('M',id,cID)}),
                andF({mst3('H',cd,cCD),notF(mst3('H',cdi,cCDI)),mst3('L',id,cID)})
            });
        // double relevance = relevantRule > notRelevantRule ? relevantRule-notRelevantRule : 0;
        return andF({relevantRule,notF(notRelevantRule)});
    }

    inline double negativeRelevanceFuzzyRules(double cd, double cdi, double id, 
    std::vector<double>& cCD,
    std::vector<double>& cCDI,
    std::vector<double>& cID){
        double relevantRule = notF(orF(
            {
                andF({mst3('L',cd,cCD),mst3('L',cdi,cCDI),mst3('H',id,cID)}),
                andF({mst3('L',cd,cCD),mst3('L',cdi,cCDI),mst3('M',id,cID)}),
                andF({mst3('L',cd,cCD),mst3('L',cdi,cCDI),mst3('L',id,cID)}),

                andF({notF(mst3('L',cd,cCD)),notF(mst3('L',cdi,cCDI)),mst3('H',id,cID)}),
                andF({notF(mst3('L',cd,cCD)),notF(mst3('L',cdi,cCDI)),mst3('M',id,cID)}),
                andF({notF(mst3('L',cd,cCD)),notF(mst3('L',cdi,cCDI)),mst3('L',id,cID)})
            }));
        return relevantRule;
    }

private:
    uint32_t window;
    uint32_t height;
    uint32_t width;
    bool isRGB;
    uint32_t tupleSize;
    std::vector<uint32_t> mapping;
    std::vector<Bin> bins;
    // temporary variables
    uint8_t* samples_img;
    uint32_t samplesTotal;
    uint8_t* base_img;
};