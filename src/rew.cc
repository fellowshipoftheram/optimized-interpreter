typedef std::string rew_addr_t;
typedef std::vector<double> regression_content_t;
typedef std::unordered_map<rew_addr_t, regression_content_t> regression_ram_t;

class RewMemory {
public:
    RewMemory(uint32_t entrySize, uint32_t tupleSize)
    :tupleSize(tupleSize),entrySize(entrySize)
    {
        blocks = tupleSize/8 + (tupleSize%8>0 ? 1 :0);

        numberOfRAMS = entrySize / tupleSize;
        int remain = entrySize % tupleSize;
        int indexesSize = entrySize;
        if(remain > 0) {
            numberOfRAMS++;
            indexesSize += tupleSize-remain;
        }


        mapping.resize(indexesSize);
        for(uint32_t i = 0; i < entrySize; i++) {
            mapping[i]=i;
        }

        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<uint32_t> dist(0, entrySize);

        for(uint32_t i = entrySize; i != mapping.size(); i++){
            mapping[i] = mapping[dist(mt)];
        }

        std::shuffle(mapping.begin(), mapping.end(), mt);
        memory = std::vector<regression_ram_t>(numberOfRAMS);
    }

    RewMemory(std::vector<uint32_t> mapping, uint32_t entrySize, uint32_t tupleSize)
    :tupleSize(tupleSize),entrySize(entrySize),mapping(mapping){
        numberOfRAMS = mapping.size()/tupleSize;
        blocks = tupleSize/8 + (tupleSize%8>0 ? 1 :0);
        memory = std::vector<regression_ram_t>(numberOfRAMS);
    }

    void train(const Bin& image, const double y) {
        for(uint32_t i=0; i!=numberOfRAMS; i++){
            rew_addr_t index = getIndex(image,i);

            auto it = memory[i].find(index);
            if (it == memory[i].end()){
                memory[i].insert(it, std::pair<rew_addr_t, regression_content_t>(index, {1, y, 0}));
            }
            else{
                it->second[0]++;
                it->second[1] += y;
            }
        }
    }

    void getMentalImage(double* mi) {
        Bin address(tupleSize);
        for(uint32_t i=0; i<entrySize; i++) mi[i]=0;

        double* y = new double[tupleSize*2];
        for(uint32_t ii=0; ii!=numberOfRAMS; ii++){
            for(uint32_t j=0; j!=tupleSize*2; j++) y[j] = 0;
            for(auto mem=memory[ii].begin(); mem!=memory[ii].end(); ++mem){
                for(uint32_t i=0; i!=tupleSize; i++){
                    char bit = (mem->first[i/8] & (1<<(i%8)));
                    if(bit!=0){
                        y[i*2] += mem->second[0]; // counter
                        y[i*2 +1] += mem->second[1]; // sum of y
                    }
                }
            }

            for(uint32_t i=0; i!=tupleSize; i++){
                if(y[i*2]!=0){
                    mi[mapping[ii*tupleSize + i]] += y[i*2 +1]/y[i*2];
                }
            }

        }
        delete[] y;
    }



private:
    inline rew_addr_t getIndex(const Bin& image, uint32_t ramId) const {
        rew_addr_t index = std::string(blocks,-1);
        uint8_t pieceIndex = -1;
        uint32_t b = 0;
        uint32_t countChar=0;
        uint32_t start = ramId*tupleSize;
        for(uint32_t i=start; i!=tupleSize+start; i++){
            uint32_t j = i-start; 
            if(image[mapping[i]]==0) {
                pieceIndex &= ~(1<<(j%8));
            }
            countChar++;
            if(countChar==8||j==tupleSize-1){
                index[b++]=pieceIndex;
                pieceIndex=-1;
                countChar=0;
            }
        }
        return index;
    }

private:
    uint32_t tupleSize;
    uint32_t blocks;
    uint32_t numberOfRAMS;
    uint32_t entrySize;
    std::vector<uint32_t> mapping;

    std::vector<regression_ram_t> memory;
};