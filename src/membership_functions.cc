inline double mst3(char type, double x, std::vector<double>& c){
    double out =0;
    switch(type){
        case 'H':
            out = x<c[1] ? 0 : (x>c[2] ? 1 : (x-c[1])/(c[2]-c[1]));
            break;
        case 'M':
            out = (x<c[0] || x>c[2]) ? 0 : ( x<c[1] ? (x-c[0])/(c[1]-c[0]) : (c[2]-x)/(c[2]-c[1]) );
            break;
        case 'L':
            out = x<c[0] ? 1 : (x>c[1] ? 0 : (c[1]-x)/(c[1]-c[0]));
            break;
    }
    return out;
}

inline double mst5(char type, double x, std::vector<double>& c){
    double out =0;
    switch(type){
        case 'H':
            out = x<c[3] ? 0 : (x>c[4] ? 1 : (x-c[3])/(c[4]-c[3]));
            break;
        case 'h':
            out = (x<c[2] || x>c[4]) ? 0 : ( x<c[3] ? (x-c[2])/(c[3]-c[2]) : (c[4]-x)/(c[4]-c[3]) );
            break;
        case 'M':
            out = (x<c[1] || x>c[3]) ? 0 : ( x<c[2] ? (x-c[1])/(c[2]-c[1]) : (c[3]-x)/(c[3]-c[2]) );
            break;
        case 'l':
            out = (x<c[0] || x>c[2]) ? 0 : ( x<c[1] ? (x-c[0])/(c[1]-c[0]) : (c[2]-x)/(c[2]-c[1]) );
            break;
        case 'L':
            out = x<c[0] ? 1 : (x>c[1] ? 0 : (c[1]-x)/(c[1]-c[0]));
            break;
    }
    return out;
}

inline double mssig3(char type, double x, std::vector<double>& c){
    double out=0;
    switch(type){
        case 'H':
            out = (tanh((3.0/(c[2]-c[1]))*(x-((c[2]-c[1])/2.0+c[1])))+1)/2;
            break;
        case 'M':
            if(x>c[1]){
                out = (tanh((3.0/(c[2]-c[1]))*(((c[2]-c[1])/2.0+c[1])-x))+1)/2;
            }
            else{
                out = (tanh((3.0/(c[1]-c[0]))*(x-((c[1]-c[0])/2.0+c[0])))+1)/2;
            }
            break;
        case 'L':
            out = (tanh((3.0/(c[1]-c[0]))*(((c[1]-c[0])/2.0+c[0])-x))+1)/2;
            break;
    }
    return out;
}