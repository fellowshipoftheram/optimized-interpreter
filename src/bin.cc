class Bin {
public:
  Bin():remain(0){}
  Bin(uint64_t size){
    setConfig(size);
  }

  Bin(const std::vector<short>& in){
    setConfig(in.size());
    for(unsigned int i=0; i<size(); i++){
      set(i,in[i]);
    }
  }

  void setSize(uint64_t size){
    setConfig(size);
  }

  int operator[](uint64_t index) const {
    return get(index);
  }

  int get(uint64_t index) const {
    int section = index/8;
    int sectionIndex = 7-(index%8);
    return (input[section] >> sectionIndex) & 0x01;
  }

  void set(uint64_t index, int value){
    uint64_t section = index/8;
    char sectionIndex = 7-(index%8);
    input[section] = (input[section] & ~(0x01 << sectionIndex)) | (value << sectionIndex);
  }

  uint64_t size() const {
    return input.size()*8-remain;
  }

  void print() const {
    std::cout << " [ ";
    int bits = input.size()*8-remain;
    for(int i=0; i!=bits; i++){
      std::cout << ((input[i/8] & 1<<(7-i%8)) == 0 ? 0 : 1) << " ";
    }
    std::cout << "] " << std::endl;
  }
  
private:
  char remain;
  std::string input;

  void setConfig(uint64_t size, char value=0){
    remain = (char)(size%8)==0?0:8-(size%8);
    input = std::string((size/8)+(size%8 == 0?0:1),value);
  }
};
