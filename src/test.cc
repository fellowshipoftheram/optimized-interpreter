#include "xaiwann.h"

void testKmeans(){
    std::vector<double> data = {
        2,9,54,6,78,8,4,5,4,8,1,6,52,1,3,5,8,35
    };
    std::vector<double> clusters = kmeans(data, 3);
    double a[] = {
        2.5, 6+3.0/9, 39+3.0/9
    };
    for(uint32_t i=0; i!=3; i++){
        if(clusters[i]!=a[i])
            print("test kmeans first stage fail: at position",i,"expected value",a[i],"received value",clusters[i]);
    }
    clusters = kmeans(data, 3, 0.001);
    double b[] = {
        2.5, 6.875, 54.75
    };
    for(uint32_t i=0; i!=3; i++){
        if(clusters[i]!=b[i])
            print("test kmeans complete fail: at position",i,"expected value",b[i],"received value",clusters[i]);
    }
}

void testRew(){
    uint32_t tupleSize;
    std::vector<uint32_t> mapping = getNeighborhoodMapping(tupleSize, 2,2,1);
    RewMemory rew(mapping,tupleSize,4);

    Bin b({0,0,1,1});
    rew.train(b,0.3);
    Bin c({1,1,0,0});
    rew.train(c,0.7);

    Bin d({0,1,1,0});
    rew.train(d,0.5);

    double mi[] = {0,0,0,0};
    rew.getMentalImage(mi);
    double expected[] = {6.3,5.4,3.6,2.7};
    for(uint32_t i=0; i!=4; i++){
        if((mi[i]-expected[i])==0)
            print("test rew fail: at position",i,"expected value",expected[i],"received value",mi[i]);
    }
}

void testNormalizations(){
    //normBiggerLesser
    double mi[] = {
        1, 2, 5, 3, 7
    };
    normBiggerLesser(&mi[0],5);
    double a[] = {
        0,1.0/6,4.0/6,2.0/6,1
    };
    for(uint32_t i=0; i!=5; i++){
        if(mi[i]!=a[i])
            print("test norm bigger lesser fail at position:",i,"expected value",a[i]," received value",mi[i]);
    }

    //l2norm2v
    double x[] = {
        1,2,3,4,5
    };
    double y[] = {
        5,4,3,2,1
    };
    double b = l2norm2v(x,y,5);
    const double c = sqrt(40);
    if(b!=c)
        print("test l2 norm fail: expected value",c,"received value",b);

    //sum
    double d[] = {
        1,2,3,4,5
    };
    const double e = 15;
    b = sum(d,5);
    if(b!=e)
        print("test l2 norm fail: expected value",e,"received value",b);


}

void testRandom(){
    double ta = randdouble();
    int tb = randint(0,100);
    int ca = 0;
    int cb = 0;
    for(uint32_t i=0; i!= 100; i++){
        double a = randdouble();
        if(ta==a)
            ca++;
        ta = a;
        int b = randint(0,1000);
        if(tb==b)
            cb++;
        tb = b;
    }
    if(ca>10)
        print("test random fail maybe randdouble is not random enough");
    if(cb>10)
        print("test random fail maybe randint is not random enough");
}

void testMembershipFunctions(){
    std::vector<double> clusters = {0.3,0.5,0.7};

    std::vector<char> parameters_type = {
        'H', 'H', 'H',
        'M', 'M', 'M', 'M',
        'L', 'L', 'L'
    };
    std::vector<double> parameters_value = {
        0.8, 0.4, 0.6,
        0.8, 0.2, 0.4, 0.6,
        0.6, 0.2, 0.4

    };
    std::vector<double> correct_answers = {
        1, 0, (0.6-0.5)/(0.7-0.5),
        0, 0, (0.4-0.3)/(0.5-0.3), (0.7-0.6)/(0.7-0.5),
        0, 1, (0.5-0.4)/(0.5-0.3)
    };
    if(parameters_value.size()!=parameters_value.size() || parameters_value.size()!=correct_answers.size())
        print("Error in test mst3 vectors have sizes differents");
        
    for(uint32_t i=0; i!=correct_answers.size(); i++){
        double a = mst3(parameters_type[i],parameters_value[i],clusters);
        if(a!=correct_answers[i])
            print("test membership function mst3",parameters_type[i],parameters_value[i],"fail the correct result is",correct_answers[i],"and not:",a);
    }
}

void testFuzzyOperators(){
    double a = andF({1,1,1});
    if(a!=1)
        print("test fuzzy operator 'and' fail the correct result is 1 and not:",a);

    a = andF({0,1,0});
    if(a!=0)
        print("test fuzzy operator 'and' fail the correct result is 0 and not:",a);
    
    a = andF({0.1,0.5,0.9});
    const double c = 0.1*0.5*0.9;
    if(a!=c)
        print("test fuzzy operator 'and' fail the correct result is", c, "and not:",a);
    
    a = orF({1,1,1});
    if(a!=1)
        print("test fuzzy operator 'or' fail the correct result is 1 and not:",a);
    
    a = orF({0,0,0});
    if(a!=0)
        print("test fuzzy operator 'or' fail the correct result is 0 and not:",a);

    a = orF({0.4,0.3,0.9});
    const double d = 0.958;
    if(a!=d)
        print("test fuzzy operator 'or' fail the correct result is", d, "and not:",a);
}

void testBin(){
    Bin b;
    if(b.size() != 0){
        print("test bin fail: bin size is not zero");
        return;
    }
    b.setSize(5);
    if(b.size() != 5){
        print("test bin fail: bin size is not 5");
        return;
    }
    for(uint32_t i=0; i!=5; i++){
        if(b[i] != 0)
            print("test bin fail: value is not zero at position:", i);
        b.set(i,1);
        if(b[i] != 1)
            print("test bin fail: value is not one at position:", i);
    }

    std::vector<short> v = {0,1,0,1,0,1,1,0,0};
    Bin bb(v);
    for(uint32_t i=0; i!=v.size(); i++){
        if(bb[i]!=v[i])
            print("test bin fail: expected value",v[i], "received value", bb[i]);
    }
}

void testMapping(){
    uint32_t tupleSize;
    std::vector<uint32_t> mapping = getNeighborhoodMapping(tupleSize, 3, 3, 1);
    std::vector<uint32_t> corret_mapping = {0,0,0,0,0,1,0,3,4,1,1,1,0,1,2,3,4,5,2,2,2,1,
    2,2,4,5,2,3,0,1,3,3,4,3,6,7,0,1,2,3,4,5,6,7,8,1,2,5,4,5,5,7,8,5,6,3,4,6,6,7,6,6,6,3,
    4,5,6,7,8,7,7,7,4,5,8,7,8,8,8,8,8};

    if(tupleSize != 9)
        print("mapping test fail tupleSize different of 9:",tupleSize);

    if(mapping.size() != corret_mapping.size())
        print("mapping test fail sizes differents correct:", corret_mapping.size(), "result:", mapping.size());
    for(uint32_t i=0; i!= mapping.size(); i++){
        if(mapping[i]!=corret_mapping[i]){
            print("mapping test fail answers differents at position:",i);
            print(corret_mapping);
            print(mapping);
            break;
        }
    }
    
}

void testAll(){
    uint32_t height = 28;
    uint32_t width = 28;
    uint32_t window = 4;
    uint32_t classes = 10;
    uint32_t samples = 1000;
    uint32_t size = height*width;

    FRWI frwi(window,height,width);

    uint8_t* data = new uint8_t[samples*size]();
    uint8_t* img = new uint8_t[size]();
    frwi.generatePermutations(data,samples,img,0);

    double* lmi = new double[size]();
    double* responses = new double[classes*samples]();
    double* c = new double[classes]();

    frwi.explain(lmi, responses, c, classes);


    delete[] data;
    samples = frwi.getTotalOfLocalPermutations();
    data = new uint8_t[samples*size]();
    frwi.generateLocalPermutations(data,samples,img,0);

    delete[] responses;
    responses = new double[classes*samples]();
    frwi.explain(lmi, responses, c, classes);

    delete[] data;
    delete[] img;
    delete[] lmi;
    delete[] responses;
    delete[] c;
}

void testAllRGB(){
    uint32_t height = 28;
    uint32_t width = 28;
    uint32_t window = 4;
    uint32_t classes = 10;
    uint32_t samples = 1000;
    uint32_t sizeRGB = height*width*3;
    uint32_t size = height*width;

    FRWI frwi(window,height,width,true);

    uint8_t* data = new uint8_t[samples*sizeRGB]();
    uint8_t* img = new uint8_t[sizeRGB]();
    frwi.generatePermutations(data,samples,img,0);

    double* lmi = new double[size]();
    double* responses = new double[classes*samples]();
    double* c = new double[classes]();

    frwi.explain(lmi, responses, c, classes);


    delete[] data;
    samples = frwi.getTotalOfLocalPermutations();
    data = new uint8_t[samples*sizeRGB]();
    frwi.generateLocalPermutations(data,samples,img,0);

    delete[] responses;
    responses = new double[classes*samples]();
    frwi.explain(lmi, responses, c, classes);

    delete[] data;
    delete[] img;
    delete[] lmi;
    delete[] responses;
    delete[] c;
}

void tests(){
    testNormalizations();
    testRandom();
    testFuzzyOperators();
    testMembershipFunctions();
    testBin();
    testMapping();
    testKmeans();
    testRew();
    testAll();
    testAllRGB();
}

int main(){
    srand(time(0));
    tests();
}