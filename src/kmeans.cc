std::vector<double> kmeans(std::vector<double> data, int totalClasses=3, double minDiffer=0){
    std::sort(data.begin(),data.end());
    std::vector<double> temp_clusters(totalClasses,0);
    std::vector<double> count_clusters(totalClasses,0);
    std::vector<double> clusters(totalClasses);

    int piece = data.size()/totalClasses;
    size_t current_class = 0;
    for(size_t i=0; i!=data.size(); i++){
        if(std::isnan(data[i])) continue; // ignore not a number;
        clusters[current_class] += data[i];
        count_clusters[current_class]++;
        if(count_clusters[current_class]==piece && current_class!=clusters.size()-1){
            current_class++;
        }
    }
    
    for(size_t i=0; i!=clusters.size(); i++) {
        clusters[i] /= count_clusters[i];
    }

    // just at in the first stage
    if(minDiffer==0)
        return clusters;

    double differ = 1;
    while(differ > minDiffer ){
        temp_clusters = clusters; // save past cluster
        // initiate variables
        for(size_t i=0; i!=clusters.size(); i++) clusters[i]=0;
        for(size_t i=0; i!=count_clusters.size(); i++) count_clusters[i]=0;

        // loop over data
        for(size_t i=0; i!=data.size(); i++){
            if(std::isnan(data[i])) continue; // ignore not a number;
            // classify data
            size_t temp_class = 0;
            double temp_class_value = abs(data[i]-temp_clusters[0]);
            for(size_t j=1; j!=clusters.size(); j++){
                double local_class_value = abs(temp_clusters[j]-data[i]);
                if( local_class_value < temp_class_value){
                    temp_class_value = local_class_value;
                    temp_class=j;
                }
            }
            // update clusters
            clusters[temp_class] += data[i];
            count_clusters[temp_class]++;
        }
        // final update clusters
        for(size_t i=0; i!=clusters.size(); i++) {
            clusters[i]/=count_clusters[i];
        }

        // calculate differ: the stop condition
        differ = 0;
        for(size_t i=0; i!=clusters.size(); i++){
            differ += abs(clusters[i]-temp_clusters[i]);
        }
        differ /= clusters.size();
    }
    return clusters;
}