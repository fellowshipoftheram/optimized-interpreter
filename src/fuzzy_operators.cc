inline double andF(std::vector<double> args){
    double out = 1.0;
    for(double arg: args) out *= arg;
    return out;
}

inline double orF(std::vector<double> args){
    double out = 0;
    for(double arg: args) out = out + arg - out*arg;
    return out;
}

inline double notF(double x){
    return 1-x;
}