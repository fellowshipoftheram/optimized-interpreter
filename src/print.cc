template<typename T>
void print(T value){
  std::cout << value << std::endl;
}

template<typename T>
void print(std::vector<T> values){
  std::cout << " [ ";
  for(T value: values){
    std::cout << value << " ";
  }
  std::cout << "] " << std::endl;
}

template<typename T, typename... Args>
void print(T value, Args... args){
  std::cout << value << " ";
  print(args...);
}

template<typename T>
void printarray(T* values, uint32_t length){
  std::cout << " [ ";
  for(uint32_t i=0; i!=length; i++){
    std::cout << values[i] << " ";
  }
  std::cout << "] " << std::endl;
}


void printbits(std::string& d, int bits){
  std::cout << " [ ";
  for(int i=0; i!=bits; i++){
    std::cout << ((d[i/8] & 1<<(i%8)) == 0 ? 0 : 1) << " ";
  }
  std::cout << "] " << std::endl;
}