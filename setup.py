
#!/usr/bin/env python
from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
import sys
import setuptools
from codecs import open  # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()
    f.close()

with open("src/version.h") as f:
    line = f.read()
    __version__ = line.split('"')[1]
    f.close()

__package_name__ = 'xaiwann'
__src__ = 'wrapper/xaiwann.cc'


ext_modules = [
    Extension(
        __package_name__,
        [__src__],
        include_dirs=[],
        language='c++'
    ),
]


class BuildExt(build_ext):
    """A custom build extension for adding compiler-specific options."""
    c_opts = {
        'msvc': ['/EHsc'],
        'unix': [],
    }

    if sys.platform == 'darwin':
        c_opts['unix'] += ['-stdlib=libc++', '-mmacosx-version-min=10.7']

    def build_extensions(self):
        ct = self.compiler.compiler_type
        opts = self.c_opts.get(ct, [])

        if ct == 'unix':
            opts.append('-DVERSION_INFO="%s"' % self.distribution.get_version())
        elif ct == 'msvc':
            opts.append('/DVERSION_INFO=\\"%s\\"' % self.distribution.get_version())

        for ext in self.extensions:
            ext.extra_compile_args = opts

        build_ext.build_extensions(self)

setup(
    name=__package_name__,
    ext_package=__package_name__,
    packages=[__package_name__],
    cmdclass={'build_ext': BuildExt},
    version=__version__,
    author='IAZero',
    description='A model agnostic explanation tool',
    long_description=long_description,
    long_description_content_type="text/markdown",
    ext_modules=ext_modules,
    install_requires=['wheel'],
    zip_safe=False,
    keywords = ['xai', 'interpreter', 'fuzzy', 'interpretation'],
    classifiers=[
        "License :: OSI Approved :: MIT License"
    ],
)
