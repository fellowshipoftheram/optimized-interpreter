#include "../src/xaiwann.h"
#define FRWI_SELF (static_cast<FRWI*>(self))

extern "C" {
    const char* version(){
        return __version__.c_str();
    }

    void* frwi_create(uint32_t window, uint32_t height, uint32_t width, bool isRGB){
        return static_cast<void*>(new FRWI(window, height, width, isRGB));
    }

    void frwi_destroy(void* self){
        delete FRWI_SELF;
    }

    uint32_t frwi_getTotalOfLocalPermutations(void* self){
        return FRWI_SELF->getTotalOfLocalPermutations();
    }

    uint32_t frwi_getLMISize(void* self){
        return FRWI_SELF->getLMISize();
    }


    // void frwi_generateLocalPermutations(void* self, void* data, uint32_t samples, void* img, uint8_t greyValue){
    //     FRWI_SELF->generateLocalPermutations((uint8_t*)data,samples,(uint8_t*)img,greyValue);
    // }

    void frwi_generatePermutations(void* self, void* data, uint32_t samples, void* img, uint8_t greyValue){
        FRWI_SELF->generatePermutations((uint8_t*)data,samples,(uint8_t*)img,greyValue);
    }

    void frwi_explain(void* self, void* localMentalImage, void* responses, void* c, uint32_t classes){
        FRWI_SELF->explain((double*)localMentalImage, (float*)responses, (float*)c, classes);
    }
}